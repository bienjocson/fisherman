package fishExercise;

import java.util.Arrays;

class Seafood implements cookSeafood{
	public int size;
	public String name;
	String seafoodType;
	
	int minimumSize;
	int maximumSize;
	
	final int minFishCookTypes=0;
	final int grilledCookTypeIndex=2;
	final int maxCrustaceanCookTypes=5;
	
	final int minFishIndex = 0;
	final int maxFishIndex = 2;
	final int minCrustaceanIndex = 3;
	final int maxCrustaceanIndex = 5;

	int nameIndex;
	
	public static String[] seafoodName= {"Anchovy","Tilapia","Tuna","Shrimp","Crab","Lobster"};
	public static String[] cookTypeName= {"Corned", "Fried", "Grilled","Boiled", "Steamed"};
	
	public void get()	{
		System.out.println("You are holding a raw " + size + " gram " + name); 
	}
	void setSeafoodType(String newValue)
	{
		seafoodType=newValue;
	}
	public String getSeafoodType()
	{
		return seafoodType;
	}
	public void setName(String newValue)
	{
		name=newValue;
	}
	public String getName()
	{
		return name;
	}
	public void setSize(int newValue) {
		size=newValue;
	}
	public int getSize()
	{
		return size;
	}

	public int generateSize(int minFishSize,int maxFishSize)	{
		return Main.getRandomNumber(minFishSize,maxFishSize);
	}
	public void cook(int cookType)	{
		nameIndex=Arrays.asList(seafoodName).indexOf(name);
		if((cookType>=minFishCookTypes && grilledCookTypeIndex<=2) && (nameIndex>=minFishIndex && nameIndex<=maxFishIndex) ||
			(cookType>=grilledCookTypeIndex && cookType<=maxCrustaceanCookTypes) && (nameIndex>=minCrustaceanIndex && nameIndex<=maxCrustaceanIndex))
			System.out.println("You ate a " + cookTypeName[cookType] + " " + name + " that weighed " + size + " grams when it was caught."); 	
		else
			System.out.println("You ate a Spoiled " + name + " that weighed " + size + " grams when it was caught.");
	}
}
