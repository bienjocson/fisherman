package fishExercise;
import java.util.Scanner; 

public class Main {
	public static void prompt() {
		Scanner scanner = new Scanner(System.in);
		boolean quit = false;
		int choice;
		do {
			System.out.println("1 - Cast to catch a fish.");
			System.out.println("2 - Eat a Fish.");
			System.out.println("3 - Eat a Crustacean.");
			System.out.println("4 - Check your baskets.");
			System.out.println("5 - Quit fishing.");
			System.out.println("Enter command:");
			
			choice = scanner.nextInt();
			
			switch(choice) {
				case 1:
					Fisherman.catchSeafood();
					  break;
				case 2:
					Fisherman.eatFish();
					  break;
				case 3:
					Fisherman.eatCrustacean();
					  break;
				case 4:
					Fisherman.checkBaskets();
					  break;
				case 5:
					System.out.println("Good fishing! Let's try again next time.");
					quit=true;
					break;
				default:
					System.out.println("That is not a valid command. Try again!");
					  break;
				
			}
			System.out.println();
		}
		while(!quit);
	}
	public static int getRandomNumber(int min, int max) {
	    return (int) ((Math.random() * (max - min)) + min);
	}
	public static void main(String[] args) {
		System.out.println("Hello there fisherman! What are we up to today? Fisherman V2");
		prompt();
	}
}
