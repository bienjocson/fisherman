package fishExercise;

public class Crab extends Crustacean {
	public Crab()	{
		super.minimumSize = 51;
		super.maximumSize = 200;
		super.name="Crab";
		super.size=generateSize(minimumSize,maximumSize);
	}
}
