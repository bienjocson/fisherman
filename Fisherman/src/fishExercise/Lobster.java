package fishExercise;

public class Lobster extends Crustacean {
	public Lobster() {
		super.minimumSize = 201;
		super.maximumSize = 500;
		super.name="Lobster";
		super.size=generateSize(minimumSize,maximumSize);
	}
}
